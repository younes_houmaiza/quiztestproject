package com.example;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class quizTest extends JFrame implements ActionListener {

    JLabel label ;
    JRadioButton radioButton[] = new JRadioButton[5];
    JButton btnNext,btnResult ;
    ButtonGroup bg;
    int count =0, current=0, x=1, y=1, now =0;
    int m[]= new int[10];


    quizTest(String s){
        super(s);
        label=new JLabel();
        add(label);
        bg= new ButtonGroup();
        for(int i=0;i<5 ; i++){
            radioButton[i]=new JRadioButton();
            add(radioButton[i]);
            bg.add(radioButton[i]);
        }
        btnNext =new JButton("Next");
        btnResult =new JButton("Result");
        btnResult.setVisible(false);
        btnNext.addActionListener(this);
        btnResult.addActionListener(this);
        add(btnNext);
        add(btnResult);
        setData();
        label.setBounds(30,40,450,20);
        radioButton[0].setBounds(50,80,450,20);
        radioButton[1].setBounds(50,110,200,20);
        radioButton[2].setBounds(50,140,200,20);
        radioButton[3].setBounds(50,170,200,20);
        btnNext.setBounds(100,240,100,30);
        btnResult.setBounds(270,240,100,30);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setLocation(250,100);
        setVisible(true);
        setSize(600,350);





    }
    void setData(){
        radioButton[4].setSelected(true);
        if (current==0){
            label.setText("Q1:what's the main programming language of android devolopement");
            radioButton[0].setText("java");
            radioButton[1].setText("Kotlin");
            radioButton[2].setText("C++");
            radioButton[3].setText("javascript");

        }
        if (current==1){
            label.setText("Q2:what's the default value of long variable ");
            radioButton[0].setText("0");
            radioButton[1].setText("0.0");
            radioButton[2].setText("0L");
            radioButton[3].setText("not known");

        }
        if (current==2) {
            label.setText("Q3:The main function of the CPU is to perform arithmetic and logical operations on data taken from ____");
            radioButton[0].setText("Permanent memory");
            radioButton[1].setText("Control unit");
            radioButton[2].setText("Main memory");
            radioButton[3].setText("CPU");


        }if (current==3){
            label.setText("Q4:Which of the following is associated with second generation computers?");
            radioButton[0].setText("all of these");
            radioButton[1].setText("transistors");
            radioButton[2].setText("magnetic core memory");
            radioButton[3].setText("operating core memory");

        }if (current==4){
            label.setText("Q5:____ is a memory on which data can be written only once.");
            radioButton[0].setText("ROM");
            radioButton[1].setText("PROM");
            radioButton[2].setText("EPROM");
            radioButton[3].setText("RAM");

        }if (current==5){
            label.setText("Q6:Which language is directly understood by the computer without translation program?");
            radioButton[0].setText("assembly language");
            radioButton[1].setText("high level language");
            radioButton[2].setText("basic language");
            radioButton[3].setText("machine language");

        }if (current==6){
            label.setText("Q7:Which resources are typically provided by an Infrastructure as a Service cloud computing delivery model?");
            radioButton[0].setText("middleware software stacks");
            radioButton[1].setText("applications");
            radioButton[2].setText("virtual machines");
            radioButton[3].setText("virtual private networks ");

        }if (current==7){
            label.setText("Q8:What is a correct definition of Cloud computing?");
            radioButton[0].setText(" A service offered by a Service provider, not limited by a Service Level Agreement (SLA)");
            radioButton[1].setText("A service architecture based on thin clients");
            radioButton[2].setText(" A network of globally interconnected client computers");
            radioButton[3].setText("A large pool of usable and accessible virtualised resources");

        }if (current==8){
            label.setText("Q9:Which computing feature is related to utility computing?");
            radioButton[0].setText("virtualisation");
            radioButton[1].setText("metring");
            radioButton[2].setText("multitenancy");
            radioButton[3].setText("security");

        }if (current==9){
            label.setText("Q10:What is a Virtual Private Network (VPN)?");
            radioButton[0].setText("An operating system for private network systems");
            radioButton[1].setText("A virtual network device for private purpose");
            radioButton[2].setText("A secured private cloud for a single user");
            radioButton[3].setText("A secured connection for remote access to a local area network");

        }
        label.setBounds(30,40,450,20);
        for (int i=0, j=0; i<=90 ; i+=30 ,j++){
            radioButton[j].setBounds(50,80+i,450,20);
        }
    }

    boolean checkAns() {
        if (current == 0) {
            return (radioButton[1].isSelected());
        }
        if (current == 1) {
            return (radioButton[2].isSelected());
        }
        if (current == 2) {
            return (radioButton[2].isSelected());
        }
        if (current == 3) {
            return (radioButton[0].isSelected());
        }
        if (current == 4) {
            return (radioButton[1].isSelected());
        }
        if (current == 5) {
            return (radioButton[3].isSelected());
        }
        if (current == 6) {
            return (radioButton[2].isSelected());
        }
        if (current == 7) {
            return (radioButton[3].isSelected());
        }
        if (current == 8) {
            return (radioButton[1].isSelected());
        }
        if (current == 9) {
            return (radioButton[3].isSelected());
        }
        return false;
    }



        public static void main(String[] args){
        new quizTest("simple quiz app");
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==btnNext){
            if (checkAns())
                count+=1;
            current++;
            setData();
            if (current==9){
                btnNext.setEnabled(false);
                btnResult.setVisible(true);
                btnResult.setText("Result");
            }
        }

        if (e.getActionCommand().equals("Result")){
            if (checkAns())
                count+=1;
            current++;
            JOptionPane.showMessageDialog(this,"correct answer are "+count);
            System.exit(0);
        }

    }
}
