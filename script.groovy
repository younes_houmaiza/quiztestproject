#!/usr/bin/env groovy

def buildJar() {
    echo "building the application..."
}

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS',     usernameVariable: 'USER')]) {
    sh "docker build -t youneshoumaiza/myrepo:jma-2.0 ."
    sh "echo \$PASS | docker login -u \$USER --password-stdin"
    sh "docker push youneshoumaiza/myrepo:jma-2.0"
}
}

def deployApp() {
    def dockerCMD = 'docker run -p 8080:3080 -d youneshoumaiza/myrepo:js0'
    sshagent(['ec2-server-ssh']) {
    sh "ssh -o StrictHostKeyChecking=no ec2user@3.120.33.149 ${dockerCMD}"
    }
}

return this
